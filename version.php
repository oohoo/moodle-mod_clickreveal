<?php // $Id: version.php,v 1.5.2.2 2009/03/19 12:23:11 mudrd8mz Exp $

/**
 * Code fragment to define the version of showdetail
 * This fragment is called by moodle_needs_upgrading() and /admin/index.php
 *
 * @author  Patrick Thibaudeau <patrick.thibaudeau@ualberta.ca>
 * @version $Id: version.php,v 1.5.2.2 2009/03/19 12:23:11 mudrd8mz Exp $
 * @package mod/showdetail
 */

$module->version  = 2010071221;  // The current module version (Date: YYYYMMDDXX)
$module->cron     = 0;           // Period for cron to check this module (secs)

?>
