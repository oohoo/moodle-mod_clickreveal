<?php  // $Id: view.php,v 1.6.2.3 2009/04/17 22:06:25 skodak Exp $

/**
 * This page prints a particular instance of showdetail
 *
 * @author  Patrick Thibaudeau <patrick.thibaudeau@ualberta.ca>
 * @version $Id: view.php,v 1.6.2.3 2009/04/17 22:06:25 skodak Exp $
 * @package mod/showdetail
 */

/// (Replace showdetail with the name of your module and remove this line)

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');
require_js('showdetail.js');

$id = optional_param('id', 0, PARAM_INT); // course_module ID, or
$a  = optional_param('a', 0, PARAM_INT);  // showdetail instance ID

if ($id) {
    if (! $cm = get_coursemodule_from_id('showdetail', $id)) {
        error('Course Module ID was incorrect');
    }

    if (! $course = get_record('course', 'id', $cm->course)) {
        error('Course is misconfigured');
    }

    if (! $showdetail = get_record('showdetail', 'id', $cm->instance)) {
        error('Course module is incorrect');
    }

} else if ($a) {
    if (! $showdetail = get_record('showdetail', 'id', $a)) {
        error('Course module is incorrect');
    }
    if (! $course = get_record('course', 'id', $showdetail->course)) {
        error('Course is misconfigured');
    }
    if (! $cm = get_coursemodule_from_instance('showdetail', $showdetail->id, $course->id)) {
        error('Course Module ID was incorrect');
    }

} else {
    error('You must specify a course_module ID or an instance ID');
}

require_login($course, true, $cm);

add_to_log($course->id, "showdetail", "view", "view.php?id=$cm->id", "$showdetail->id");

/// Print the page header
$strshowdetails = get_string('modulenameplural', 'showdetail');
$strshowdetail  = get_string('modulename', 'showdetail');

$navlinks = array();
$navlinks[] = array('name' => $strshowdetails, 'link' => "index.php?id=$course->id", 'type' => 'activity');
$navlinks[] = array('name' => format_string($showdetail->name), 'link' => '', 'type' => 'activityinstance');

$navigation = build_navigation($navlinks);

print_header_simple(format_string($showdetail->name), '', $navigation, '', '', true,
              update_module_button($cm->id, $course->id, $strshowdetail), navmenu($course, $cm));

/// Print the main part of the page
$showdetails = get_records('showdetail_content','showdetail_id',$showdetail->id,'content_order');
$i = 1;
foreach ($showdetails as $showdetail){

    print_box_start();
    echo '<div id="Q'.$i.'">'."\n";
    echo '<div class="hiddenText">'.$showdetail->content.'<a class="qLink" href="javascript:ShowQResponse(\'Q'.$i.'\', 2);">...'.$showdetail->key_word_show.'</a> </div>'."\n";
    echo '<div style="DISPLAY: none" class="hiddenText">'.$showdetail->content_details.'<a class="qLink1" href="javascript:ShowQResponse(\'Q'.$i.'\',1);"> '.$showdetail->key_word_hide.'</a> </div>'."\n";
    echo '</div>'."\n";
    $i++;
    print_box_end();
    
}


/// Finish the page
print_footer($course);

?>
