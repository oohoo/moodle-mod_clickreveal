<?php // $Id: index.php,v 1.9.9.1 2010/07/12 15:00:00Exp $

/**
 * This page lists all the instances of showdetail in a particular course
 *
 * @author  Patrick Thibaudeau <patrick.thibaudeau@ualberta.ca>
 * @version $Id: index.php,v 1.9.9.1 2010/07/12 15:00:00Exp $
 * @package mod/showdetail
 */

/// Replace showdetail with the name of your module and remove this line

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');

$id = required_param('id', PARAM_INT);   // course

if (! $course = get_record('course', 'id', $id)) {
    error('Course ID is incorrect');
}

require_course_login($course);

add_to_log($course->id, 'showdetail', 'view all', "index.php?id=$course->id", '');


/// Get all required stringsshowdetail

$strshowdetails = get_string('modulenameplural', 'showdetail');
$strshowdetail  = get_string('modulename', 'showdetail');


/// Print the header

$navlinks = array();
$navlinks[] = array('name' => $strshowdetails, 'link' => '', 'type' => 'activity');
$navigation = build_navigation($navlinks);

print_header_simple($strshowdetails, '', $navigation, '', '', true, '', navmenu($course));

/// Get all the appropriate data

if (! $showdetails = get_all_instances_in_course('showdetail', $course)) {
    notice('There are no instances of showdetail', "../../course/view.php?id=$course->id");
    die;
}

/// Print the list of instances (your module will probably extend this)

$timenow  = time();
$strname  = get_string('name');
$strweek  = get_string('week');
$strtopic = get_string('topic');

if ($course->format == 'weeks') {
    $table->head  = array ($strweek, $strname);
    $table->align = array ('center', 'left');
} else if ($course->format == 'topics') {
    $table->head  = array ($strtopic, $strname);
    $table->align = array ('center', 'left', 'left', 'left');
} else {
    $table->head  = array ($strname);
    $table->align = array ('left', 'left', 'left');
}

foreach ($showdetails as $showdetail) {
    if (!$showdetail->visible) {
        //Show dimmed if the mod is hidden
        $link = '<a class="dimmed" href="view.php?id='.$showdetail->coursemodule.'">'.format_string($showdetail->name).'</a>';
    } else {
        //Show normal if the mod is visible
        $link = '<a href="view.php?id='.$showdetail->coursemodule.'">'.format_string($showdetail->name).'</a>';
    }

    if ($course->format == 'weeks' or $course->format == 'topics') {
        $table->data[] = array ($showdetail->section, $link);
    } else {
        $table->data[] = array ($link);
    }
}

print_heading($strshowdetails);
print_table($table);

/// Finish the page

print_footer($course);

?>
