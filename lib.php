<?php  // $Id: lib.php,v 1.7.2.5 2009/04/22 21:30:57 skodak Exp $

/**
 * Library of functions and constants for module showdetail
 * This file should have two well differenced parts:
 *   - All the core Moodle functions, neeeded to allow
 *     the module to work integrated in Moodle.
 *   - All the showdetail specific functions, needed
 *     to implement all the module logic. Please, note
 *     that, if the module become complex and this lib
 *     grows a lot, it's HIGHLY recommended to move all
 *     these module specific functions to a new php file,
 *     called "locallib.php" (see forum, quiz...). This will
 *     help to save some memory when Moodle is performing
 *     actions across all modules.
 */

/// (replace showdetail with the name of your module and delete this line)

$showdetail_EXAMPLE_CONSTANT = 42;     /// for example
function showdetail_get_types() {
global $CFG;

$types = array();

$type = new object();
$type->modclass = MOD_CLASS_RESOURCE;
$type->type = 'showdetail';
$type->typestr = get_string('add', 'showdetail');
$types[] = $type;

return $types;
}

/**
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @param object $showdetail An object from the form in mod_form.php
 * @return int The id of the newly inserted showdetail record
 */
function showdetail_add_instance($showdetail) {

    $showdetail->timecreated = time();
//insert tabs and content
    if ($showdetail->id = insert_record('showdetail', $showdetail)) {
        foreach ($showdetail->content as $key => $value) {
            $value = trim($value);
            if (isset($value) && $value <> '') {
                $option = new object();
                $option->content = $value;
                $option->showdetail_id = $showdetail->id;
                if (isset($showdetail->content_details[$key])) {
                    $option->content_details = $showdetail->content_details[$key];
                }
                if (isset($showdetail->key_word_show[$key])) {
                    $option->key_word_show = $showdetail->key_word_show[$key];
                }
                if (isset($showdetail->key_word_hide[$key])) {
                    $option->key_word_hide = $showdetail->key_word_hide[$key];
                }
                if (isset($showdetail->content_order[$key])) {
                    $option->content_order = $showdetail->content_order[$key];
                }
                $option->timemodified = time();
                insert_record("showdetail_content", $option);
            }
        }
    }
    return $showdetail->id;
}


/**
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @param object $showdetail An object from the form in mod_form.php
 * @return boolean Success/Fail
 */
function showdetail_update_instance($showdetail) {

    $showdetail->timemodified = time();
    $showdetail->id = $showdetail->instance;

    foreach ($showdetail->content as $key => $value) {
        $value = trim($value);
        $option = new object();
        $option->content = $value;
	$option->content_details = $showdetail->content_details[$key];
	$option->key_word_show = $showdetail->key_word_show[$key];
        $option->key_word_hide = $showdetail->key_word_hide[$key];
        $option->content_order = $showdetail->content_order[$key];
	$option->showdetail_id = $showdetail->id;
        $option->timemodified = time();
        if (isset($showdetail->optionid[$key]) && !empty($showdetail->optionid[$key])){//existing tab record
            $option->id=$showdetail->optionid[$key];
            if (isset($value) && $value <> '') {
                update_record("showdetail_content", $option);
            } else { //empty old option - needs to be deleted.
                delete_records("showdetail_content", "id", $option->id);
            }
        } else {
            if (isset($value) && $value <> '') {
                insert_record("showdetail_content", $option);
            }
        }
    }

    return update_record('showdetail', $showdetail);
}


/**
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @param int $id Id of the module instance
 * @return boolean Success/Failure
 */
function showdetail_delete_instance($id) {

    if (! $showdetail = get_record('showdetail', 'id', $id)) {
        return false;
    }

    $result = true;

    # Delete any dependent records here #

    if (! delete_records('showdetail', 'id', $showdetail->id)) {
        $result = false;
    }
    if (! delete_records('showdetail_content', 'showdetail_id', $showdetail->id)) {
        $result = false;
    }

    return $result;
}


/**
 * Return a small object with summary information about what a
 * user has done with a given particular instance of this module
 * Used for user activity reports.
 * $return->time = the time they did it
 * $return->info = a short text description
 *
 * @return null
 * @todo Finish documenting this function
 */
function showdetail_user_outline($course, $user, $mod, $showdetail) {
    return $return;
}


/**
 * Print a detailed representation of what a user has done with
 * a given particular instance of this module, for user activity reports.
 *
 * @return boolean
 * @todo Finish documenting this function
 */
function showdetail_user_complete($course, $user, $mod, $showdetail) {
    return true;
}


/**
 * Given a course and a time, this module should find recent activity
 * that has occurred in showdetail activities and print it out.
 * Return true if there was output, or false is there was none.
 *
 * @return boolean
 * @todo Finish documenting this function
 */
function showdetail_print_recent_activity($course, $isteacher, $timestart) {
    return false;  //  True if anything was printed, otherwise false
}


/**
 * Function to be run periodically according to the moodle cron
 * This function searches for things that need to be done, such
 * as sending out mail, toggling flags etc ...
 *
 * @return boolean
 * @todo Finish documenting this function
 **/
function showdetail_cron () {
    return true;
}


/**
 * Must return an array of user records (all data) who are participants
 * for a given instance of showdetail. Must include every user involved
 * in the instance, independient of his role (student, teacher, admin...)
 * See other modules as example.
 *
 * @param int $showdetailid ID of an instance of this module
 * @return mixed boolean/array of students
 */
function showdetail_get_participants($showdetailid) {
    return false;
}


/**
 * This function returns if a scale is being used by one showdetail
 * if it has support for grading and scales. Commented code should be
 * modified if necessary. See forum, glossary or journal modules
 * as reference.
 *
 * @param int $showdetailid ID of an instance of this module
 * @return mixed
 * @todo Finish documenting this function
 */
function showdetail_scale_used($showdetailid, $scaleid) {
    $return = false;

    //$rec = get_record("showdetail","id","$showdetailid","scale","-$scaleid");
    //
    //if (!empty($rec) && !empty($scaleid)) {
    //    $return = true;
    //}

    return $return;
}


/**
 * Checks if scale is being used by any instance of showdetail.
 * This function was added in 1.9
 *
 * This is used to find out if scale used anywhere
 * @param $scaleid int
 * @return boolean True if the scale is used by any showdetail
 */
function showdetail_scale_used_anywhere($scaleid) {
    if ($scaleid and record_exists('showdetail', 'grade', -$scaleid)) {
        return true;
    } else {
        return false;
    }
}


/**
 * Execute post-install custom actions for the module
 * This function was added in 1.9
 *
 * @return boolean true if success, false on error
 */
function showdetail_install() {
    return true;
}


/**
 * Execute post-uninstall custom actions for the module
 * This function was added in 1.9
 *
 * @return boolean true if success, false on error
 */
function showdetail_uninstall() {
    return true;
}


//////////////////////////////////////////////////////////////////////////////////////
/// Any other showdetail functions go here.  Each of them must have a name that
/// starts with showdetail_
/// Remember (see note in first lines) that, if this section grows, it's HIGHLY
/// recommended to move all funcions below to a new "localib.php" file.


?>
