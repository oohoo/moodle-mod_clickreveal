<?php //$Id: mod_form.php,v 1.2.2.3 2009/03/19 12:23:11 mudrd8mz Exp $

/**
 * This file defines the main showdetail configuration form
 * It uses the standard core Moodle (>1.8) formslib. For
 * more info about them, please visit:
 *
 * http://docs.moodle.org/en/Development:lib/formslib.php
 *
 * The form must provide support for, at least these fields:
 *   - name: text element of 64cc max
 *
 * Also, it's usual to use these fields:
 *   - intro: one htmlarea element to describe the activity
 *            (will be showed in the list of activities of
 *             showdetail type (index.php) and in the header
 *             of the showdetail main page (view.php).
 *   - introformat: The format used to write the contents
 *             of the intro field. It automatically defaults
 *             to HTML when the htmleditor is used and can be
 *             manually selected if the htmleditor is not used
 *             (standard formats are: MOODLE, HTML, PLAIN, MARKDOWN)
 *             See lib/weblib.php Constants and the format_text()
 *             function for more info
 */

require_once($CFG->dirroot.'/course/moodleform_mod.php');

class mod_showdetail_mod_form extends moodleform_mod {

    function definition() {

        global $COURSE;
        $mform =& $this->_form;

//-------------------------------------------------------------------------------
    /// Adding the "general" fieldset, where all the common settings are showed
        $mform->addElement('header', 'general', get_string('general', 'form'));

    /// Adding the standard "name" field
        $mform->addElement('text', 'name', get_string('name', 'showdetail'), array('size'=>'64'));
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, '');

    /// Adding the required "description" field to hold the description of the instance
        $mform->addElement('htmleditor', 'description', get_string('description', 'showdetail'));
        $mform->setType('description', PARAM_RAW);
        $mform->setHelpButton('description', array('writing', 'richtext'), false, 'editorhelpbutton');

    /// Create repeat array count for ordering
        $repeatnum=count_records('showdetail_content', 'showdetail_id', $this->_instance);

        if ($repeatnum == 0){
        $repeatnum = count($_POST['optionid']);
        }
        for ($i=1; $i <= $repeatnum+1; $i++){
            if ($i == 1){
              $order = 1;
            } else {
              $order = $order.','.$i ;
            }

        }

        $contentorderarray = explode(',',$order);
    /// Adding the content form

        $repeatarray=array();

        $repeatarray[] = $mform->createElement('header', 'Content', get_string('contentheader','showdetail').' {no}');
        $repeatarray[] = $mform->createElement('htmleditor', 'content', get_string('content','showdetail'),array('size'=>'65'));
        $repeatarray[] = $mform->createElement('htmleditor', 'content_details', get_string('content_details','showdetail'),array('size'=>'65'));
	$repeatarray[] = $mform->createElement('text', 'key_word_show', get_string('keywordshow','showdetail'));
        $repeatarray[] = $mform->createElement('text', 'key_word_hide', get_string('keywordhide','showdetail'));
        $repeatarray[] = $mform->createElement('select', 'content_order', get_string('order','showdetail'),$contentorderarray);
        $repeatarray[] = $mform->createElement('hidden', 'optionid', 0);


        if ($this->_instance){
            $repeatno=count_records('showdetail_content', 'showdetail_id', $this->_instance);
            $repeatno += 1;
        } else {
            $repeatno = 1;
        }



	$repeateloptions = array();
        if (!isset($repeateloptions['content_order']));
        $repeateloptions['content_order']['default'] = $i-2;
        $repeateloptions['key_word_show']['default'] = get_string('default_keyword_show','showdetail');
        $repeateloptions['key_word_hide']['default'] = get_string('default_keyword_hide','showdetail');
        $mform->setType('content_order', PARAM_INT);
        $mform->setType('key_word_show', PARAM_TEXT);
        $mform->setType('key_word_hide', PARAM_TEXT);
        $mform->setType('optionid', PARAM_INT);



        $this->repeat_elements($repeatarray, $repeatno, $repeateloptions, 'option_repeats','option_add_fields', 1, get_string('addcontent','showdetail'));


//-------------------------------------------------------------------------------
        // add standard elements, common to all modules
        $this->standard_coursemodule_elements();
//-------------------------------------------------------------------------------
        // add standard buttons, common to all modules
        $this->add_action_buttons();

    }
    function data_preprocessing(&$default_values){

        if (!empty($this->_instance) && ($options = get_records('showdetail_content','showdetail_id', $this->_instance,'content_order'))) {

           // print_object($options);

            $showdetailids=array_keys($options);
            $options=array_values($options);


            foreach (array_keys($options) as $key){
                $default_values['content['.$key.']'] = $options[$key]->content;
		$default_values['content_details['.$key.']'] = $options[$key]->content_details;
		$default_values['key_word_show['.$key.']'] = $options[$key]->key_word_show;
                $default_values['key_word_hide['.$key.']'] = $options[$key]->key_word_hide;
                $default_values['content_order['.$key.']'] = $options[$key]->content_order;
		$default_values['optionid['.$key.']'] = $showdetailids[$key];
            }

        }


    }
}

?>
