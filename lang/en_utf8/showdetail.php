<?php
$string['add'] = 'Add new Click & reveal';
$string['addcontent'] = 'Add new content';
$string['content'] = 'Enter partial text';
$string['content_details'] = 'Enter full text to be revealed';
$string['contentheader'] = 'Content';
$string['default_keyword_show'] = 'more';
$string['default_keyword_hide'] = 'less';
$string['description'] = 'Description';
$string['keywordshow'] = 'Enter the word you would like to use to show the content details';
$string['keywordhide'] = 'Enter the word you would like to use to hide the content details';
$string['order'] = 'The order you would like the content in';
$string['showdetail'] = 'Click and reveal';
$string['modulename'] = 'Click and reveal';
$string['modulenameplural'] = 'Click and reveals';
$string['name'] = 'Name';

?>
